import { basename } from 'path';
import postgres from './postgres';

const isCriticalProcess = basename(process.mainModule.filename) === 'index.js';
const DELAY_TIME = 60000; // time to wait before kill process
let timeout;

/**
 * Error handle
 */
const errorHandler = (err, isCriticalConnection = false) => {
    if (err) {
        if (isCriticalProcess && !isCriticalConnection) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(() => {
                console.log(process.pid);
                process.kill(process.pid, 'SIGUSR1');
            }, DELAY_TIME);
            return null;
        }
        process.kill(process.pid, 'SIGUSR1');
    }
    return err;
};

/**
 * Disconnect to database
 */
const disconnect = async () => {
    try {
        await postgres.disconnect();
    } catch (error) {
        console.log(`postgres disconnect: ${error}`);
    }
    await new Promise((s) => setTimeout(s, 100));
};

/**
 * Create connection to database
 */
const connect = async () => {
    try {
        await postgres.connect((err) => {
            console.log(`Connection to Postgres error: ${err}`);
            errorHandler(err);
        });
    } catch (error) {
        if (errorHandler(error)) {
            throw error;
        }
    }
};

module.exports = {
    connect,
    disconnect
};
