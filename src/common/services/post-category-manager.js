import { Op } from 'sequelize';
import httpStatus from 'http-status';
// Models
import eventBus from './event-bus';
import PostCategory from '../models/post-category.model';
import APIError from '../utils/APIError';


function registerPostCategoryEvent() {
    /**
     * Nếu xóa category cha thì sẽ xóa các categories con nếu như categories không có sản phẩm
     */
    eventBus.on(PostCategory.Events.POST_CATEGORY_DELETED, async (data) => {
        try {
            const { id } = data.dataValues;
            await PostCategory.destroy({
                where: {
                    path: {
                        [Op.contains]: [id]
                    }
                }
            });
        } catch (error) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Cannot remove post category'
            });
        }
    });
}

module.exports = {
    registerPostCategoryEvent
};
