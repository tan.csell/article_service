import collectionEvent from '../services/collection.manager';

export default {
    register: () => {
        collectionEvent.registerCollectionEvent();
    }
};
