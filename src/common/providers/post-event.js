import postEvent from '../services/post.manager';

export default {
    register: () => {
        postEvent.registerPostEvent();
    }
};
