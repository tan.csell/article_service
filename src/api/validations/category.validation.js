import Joi from 'joi';

module.exports = {
    // GET /v1/categories
    listValidation: {
        query: {
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .min(1)
                .max(1000)
                .default(20),
            nested: Joi.bool(),
            slug: Joi.string(),
            is_active: Joi.bool(),
            keyword: Joi.string()
                .allow(null, '')
        }
    },

    // POST /v1/categories
    createValidation: {
        body: {
            name: Joi.string()
                .max(255)
                .trim()
                .required(),
            note: Joi.string()
                .allow(null, ''),
            slug: Joi.string()
                .max(255)
                .trim()
                .allow('', null),
            logo: Joi.string()
                .trim()
                .allow('', null),
            image: Joi.string()
                .max(255)
                .trim()
                .allow('', null),
            parent_id: Joi.number()
                .allow(null),
            position: Joi.number()
                .allow(null),

            // configuration
            is_active: Joi.bool(),
        }
    },

    // PUT /v1/categories/:categoryId
    updateValidation: {
        params: {
            id: Joi.number()
                .required()
        },
        body: {
            name: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            note: Joi.string()
                .allow(null, ''),
            slug: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            logo: Joi.string()
                .trim()
                .allow('', null),
            image: Joi.string()
                .max(255)
                .trim()
                .allow('', null),
            position: Joi.number()
                .allow(null),
            // configuration
            is_active: Joi.bool(),
        }
    }
};
