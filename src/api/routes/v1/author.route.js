import express from 'express';
import validate from 'express-validation';
import controller from '../../controllers/v1/author.controller';
import middleware from '../../middlewares/author.middleware';
import * as validation from '../../validations/author.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(validation.listValidation),
        middleware.count,
        controller.list
    )
    .post(
        validate(validation.createValidation),
        middleware.prepareParams,
        controller.create
    );

router
    .route('/:id')
    .get(
        validate(validation.detailValidation),
        middleware.load,
        controller.detail
    )
    .put(
        validate(validation.updateValidation),
        middleware.load,
        middleware.prepareUpdate,
        controller.update
    )
    .delete(
        validate(validation.deleteValidation),
        middleware.load,
        controller.delete
    );

export default router;

