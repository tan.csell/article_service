import express from 'express';
import postRoutes from './post.route';
import postCategoryRoutes from './category.route';
import collectionRoutes from './collection.route';
import authorRoutes from './author.route';

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'));

router.get('/version/:service', (req, res) =>
    res.send(process.env.GIT_COMMIT_TAG || 'Not available')
);

router.use('/posts', postRoutes);
router.use('/post-categories', postCategoryRoutes);
router.use('/collections', collectionRoutes);
router.use('/authors', authorRoutes);

export default router;
