import express from 'express';
import validate from 'express-validation';
import controller from '../../controllers/v1/category.controller';
import middleware from '../../middlewares/category.middleware';

import {
    listValidation,
    createValidation,
    updateValidation
} from '../../validations/category.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listValidation),
        middleware.count,
        controller.list
    )
    .post(
        validate(createValidation),
        middleware.prepareParams,
        controller.create
    );

router
    .route('/:id')
    .get(
        middleware.load,
        controller.detail
    )
    .put(
        validate(updateValidation),
        middleware.load,
        controller.update
    )
    .delete(
        middleware.load,
        middleware.prepareDelete,
        controller.delete
    );

export default router;

